#!/usr/bin/env python3

import os
import re
import bs4
import glob
import json
import math
import datetime
import requests

if len(glob.glob('usatf*.html')) == 0:
    show = 1000
    page = 1
    maxpage = 1 # failsafe
    while page <= maxpage:
        r = requests.get('http://www.usatf.org/calendars/searchResults.asp', params = {
            'show': show,
            'page': page,
        })
        if page == 1:
            r_s = bs4.BeautifulSoup(r.text, 'html.parser')
            matches = r_s.find('div', { 'id': 'centerMain' }).find_all('p')[2].text.split()[3]
            maxpage = math.ceil(float(matches) / show)
        fname = 'usatf{}.html'.format(show * page)
        with open(fname, 'w') as f:
            f.write(r.text)
            print('wrote {}'.format(fname))
        page += 1
print('checked search pages')

for page in glob.glob('usatf*.html'):
    page_s = bs4.BeautifulSoup(open(page, 'r').read(), 'html.parser')
    cal_s = page_s.find('table', { 'class': 'calendar' })
    for ev_s in cal_s.find_all('tr', { 'id': False })[1:]:
        tds = ev_s.find_all('td')
        eventID = tds[2].find('a')['href'].split('\'')[1]
        fname = 'infos/{}_i.html'.format(eventID)
        if not os.path.exists(fname):
            r = requests.get('http://www.usatf.org/calendars/searchResults_info.asp', params = {
                'eventID': eventID,
            })
            if r.status_code != 200:
                print('status code: {} {}\n'.format(r.status_code, eventID))
                exit()
            with open(fname, 'w') as f:
                f.write(r.text)
                print('wrote {}'.format(fname))
print('checked info pages')

for info in glob.glob('infos/*_i.html'):
    fname = 'infos/{}_e.html'.format(info.split('/')[1].split('_')[0])
    if not os.path.exists(fname):
        info_s = bs4.BeautifulSoup(open(info, 'r').read(), 'html.parser')
        itab_s = info_s.find('table', { 'xmlns:custom': 'http://usatf.org/custom' })
        trs = itab_s.find_all('tr')
        td1s = trs[0].find_all('td')
        eva_s = td1s[0].find('a', href = lambda x: '/calendars/showEvents.asp?eventID=' in x)
        if eva_s:
            r = requests.get('http://www.usatf.org' + eva_s['href'].split('\'')[1])
            if r.status_code != 200:
                print('status code: {} {}\n'.format(r.status_code, eventID))
                exit()
            with open(fname, 'w') as f:
                f.write(r.text)
                print('wrote {}'.format(fname))
print('checked event pages')

ev2eventcode = {
    'long jump':              'LJ',
    'javelin throw':          'JT',
    'shot put':               'SP',
    'high jump':              'HJ',
    'half-marathon':          'HM',
    'marathon':               'MAR',
    'pole vault':             'PV',
    'triple jump':            'TJ',
    'discus throw':           'DT',
    'triathlon':              'TRI',
    'weight pentathlon':      'PENWT',
    'superweight pentathlon': 'PENWT', # TODO fix?
    'throws pentathlon':      'PENWT', # TODO fix?
    'pentathlon':             'PEN',
    'hammer throw':           'HT',
    'weight throw':           'WT',
    'superweight throw':      'SWT',
    'decathlon':              'DEC',
    'heptathlon':             'HEP',
}
def ev_to_code(ev):
    if ev in ev2eventcode:
        return ev2eventcode[ev]
    if ev.endswith(' fun run'):
        dc = ev_to_code(' '.join(ev.split()[:-2]))
        if dc: return dc + ' fun-run'
    if ev.endswith(' mountain run'):
        dc = ev_to_code(' '.join(ev.split()[:-2]))
        if dc: return dc + ' mountain-run'
    if ev.endswith(' walk') and 'race walk' not in ev:
        dc = ev_to_code(' '.join(ev.split()[:-1]))
        if dc: return dc + ' walk'
    if ev.endswith(' m relay'): # "400 m relay"
        return '4x' + str(int(int(ev.split()[0]) / 4))
    if ev.endswith(' relay'): # "100 km relay", "50 mi relay"
        dc = ev_to_code(' '.join(ev.split()[:-1]))
        if dc: return dc + ' relay'
    if ev.endswith(' race walk'): # "5 km race walk"
        dc = ev_to_code(' '.join(ev.split()[:-2]))
        if dc: return dc + 'W'
    if ev.endswith(' m'): # "100 m"
        return ev.split()[0]
    if ev.endswith(' m hurdles'): # "100 m hurdles"
        return ev.split()[0] + 'H'
    if ev.endswith(' km'): # "8 km"
        return ev.split()[0] + 'K'
    if ev.endswith(' m steeplechase'): # "3000 m steeplechase"
        return ev.split()[0] + 'SC'
    if ev.endswith('steeplechase'): # "3000 m steeplechase"
        if ev[0] == '3': # fixes errors like "3000 km steeplechase", "3000 mi steeplechase"
            return '3000SC'
        elif ev[0] == '2':
            return '2000SC'
    if ev.endswith(' mi'): # "50 mi"
        return ev.split()[0] + 'M'
    if ev.endswith(' hr'): # "4 hr"
        return ev.split()[0] + 'hr'
    if ev.endswith(' yd'): # "440 yd"
        return ev.split()[0] + 'yd'
    
for page in glob.glob('usatf*.html'):
    page_s = bs4.BeautifulSoup(open(page, 'r').read(), 'html.parser')
    cal_s = page_s.find('table', { 'class': 'calendar' })
    for ev_s in cal_s.find_all('tr', { 'id': False })[1:]:
        tds = ev_s.find_all('td')
        eventID = tds[2].find('a')['href'].split('\'')[1]
        year = datetime.datetime.strptime(tds[0].text.replace('*', '').split('/')[-1], '%y')
        year = datetime.datetime.strftime(year, '%Y')
        loc = tds[3].text.strip()
        country = loc.split(',')[2].strip() if loc.count(',') == 2 else 'USA'
        slug = tds[2].text.lower()
        slug = ''.join(c for c in slug if c.isalnum() or c == ' ')
        slug = re.sub(' +', ' ', slug).replace(' ', '_')
        jfname = 'out/{}/{}/{}.json'.format(year, country, slug)
        if not os.path.exists(jfname):
            print(jfname)
            os.makedirs(os.path.dirname(jfname), exist_ok = True)
            meet_j = {}
            meet_j['name'] = tds[2].text.strip()
            date = ''.join([ c for c in tds[0].text.strip() if c.isdigit() or c in [ '/', '-' ] ])
            if '-' in date:
                if date.count('/') == 4: # new year edgecase e.g. "11/11/17-5/26/18"
                    start = date.split('-')[0]
                    end = date.split('-')[1]
                elif date.count('/') == 3: # e.g. "8/30-9/3/18"
                    start = date.split('-')[0] + '/' + date.split('/')[-1]
                    end = date.split('-')[1]
                elif date.count('/') == 2: # e.g. "7/12-15/18"
                    start = date.split('-')[0] + '/' + date.split('/')[-1]
                    end = date.split('/')[0] + '/' +  date.split('-')[1]
                else:
                    print('{}: couldn\'t parse date range'.format(jfname))
                    exit()
            else: # e.g. "7/12/18"
                start = end = date
            start_t = datetime.datetime.strptime(start, '%m/%d/%y')
            end_t = datetime.datetime.strptime(end, '%m/%d/%y')
            meet_j['startDate'] = datetime.datetime.strftime(start_t, '%Y-%m-%d')
            meet_j['endDate'] = datetime.datetime.strftime(end_t, '%Y-%m-%d')
            meet_j['year'] = datetime.datetime.strftime(start_t, '%Y')
            loc = tds[3].text.strip()
            if loc.count(',') == 2: # country specified e.g. "London, United Kingdom, GBR"
                meet_j['country'] = loc.split(',')[2].strip()
                meet_j['state'] = loc.split(',')[1].strip()
                meet_j['city'] = loc.split(',')[0].strip()
            else:
                meet_j['country'] = 'USA'
                meet_j['state'] = loc.split(',')[1].strip()
                meet_j['city'] = loc.split(',')[0].strip()
            if tds[1].find('img') is not None: # usatf certified
                meet_j['usatfCertified'] = True
            else:
                meet_j['usatfCertified'] = False
            ifname = 'infos/{}_i.html'.format(eventID)
            if os.path.exists(ifname):
                info_s = bs4.BeautifulSoup(open(ifname, 'r').read(), 'html.parser')
                itab_s = info_s.find('table', { 'xmlns:custom': 'http://usatf.org/custom' })
                media_s = itab_s.find('table', { 'class': 'media' })
                if media_s: # url exists
                    meet_j['url'] = media_s.find('a')['href']
                trs = itab_s.find_all('tr')
                td1s = trs[0].find_all('td')
                if len(td1s[0].find_all('p')) > 1: # description exists
                    meet_j['description'] = td1s[0].find_all('p')[1].text.strip()
                contact_s = itab_s.find('td', { 'class': 'contact' })
                if contact_s: # RD exists
                    meet_j['director'] = {}
                    rds = contact_s.contents
                    meet_j['director']['name'] = str(rds[2]).strip()
                    for rd in rds:
                        if 'phone:' in str(rd):
                            phone = str(rds[6]).split('phone:')[1].strip()
                            if phone != '000-000-0000':
                                meet_j['director']['phone'] = phone
                            break
                    obmail = contact_s.find('a')['href'].split('\'')[1]
                    meet_j['director']['email'] = obmail.split(';')[1] + '@' + obmail.split(';')[0]
            efname = 'infos/{}_e.html'.format(eventID)
            if os.path.exists(efname):
                events_s = bs4.BeautifulSoup(open(efname, 'r').read(), 'html.parser')
                meet_j['events'] = []
                calt_s = events_s.find('table', { 'class': 'calendar' })
                head_s = calt_s.find_all('tr')[0]
                evno = ageno = sexno = roundno = timeno = ddno = None
                for i, td in enumerate(head_s.find_all('td'), start = 0):
                    if td.text == 'Event':
                        evno = i
                    elif td.text == 'Sex':
                        sexno = i
                    elif td.text == 'Age Group':
                        ageno = i
                    elif td.text == 'Round':
                        roundno = i
                    elif td.text == 'Date/Time':
                        timeno = i
                    elif td.text == 'Disabled Division':
                        ddno = i
                    else:
                        print('unknown column: {}'.format(td))
                for ev_s in calt_s.find_all('tr')[1:]:
                    ev_j = {}
                    if len(ev_s.find_all('td', { 'colspan': True })) > 0: # bogus date row
                        continue # todo: use date here
                    tds = ev_s.find_all('td')
                    if evno is not None and tds[evno].text != '':
                        ename = tds[evno].text
                        ev_j['eventName'] = ename
                        ecode = ev_to_code(ename)
                        if ecode is not None:
                            ev_j['eventCode'] = ecode
                        else:
                            print('unrecognized event {}'.format(ename))
                    if sexno is not None and tds[sexno].text != '':
                        if tds[sexno].text == 'women':
                            ev_j['sex'] = 'F'
                        elif tds[sexno].text == 'men':
                            ev_j['sex'] = 'M'
                        else:
                            print('unrecognized gender {}'.format(tds[sexno].text))
                    if roundno is not None and tds[roundno].text != '':
                        ev_j['round'] = tds[roundno].text
                    if ddno is not None and tds[ddno].text != '':
                        if tds[ddno].text == 'wheelchair':
                            ev_j['category'] = 'wheelchair'
                    if timeno is not None and tds[timeno].text not in [ '', 'NaN p.m.' ]:
                        # format "7:00 a.m." or "NaN p.m."
                        times = tds[timeno].text.replace('a.m.', 'AM').replace('p.m.', 'PM')
                        dt = datetime.datetime.strptime(times, '%I:%M %p')
                        ev_j['time'] = datetime.datetime.strftime(dt, '%H:%M')
                    if ageno is not None and tds[ageno].text.strip() != '':
                        ev_j['ageGroups'] = tds[ageno].text.strip()
                    meet_j['events'].append(ev_j)
            with open(jfname, 'w') as f:
                json.dump(meet_j, f)

# http://www.usatf.org/events/courses/maps/showMap.asp?courseID=NV09004DCR
